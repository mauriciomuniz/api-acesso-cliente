package br.com.mastertech.cliente.controller;

import br.com.mastertech.cliente.DTO.ClientDTO;
import br.com.mastertech.cliente.model.Client;
import br.com.mastertech.cliente.service.ClientService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClientController {

    @Autowired
    public ClientService clientService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Client cadastrarClientNoSistema(@RequestBody @Valid ClientDTO client) {
        return clientService.cadastrar(client);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Client consultarClientePorId(@PathVariable(name = "id") int id) throws NotFoundException {
        return clientService.consultar(id);
    }
}
