package br.com.mastertech.cliente.service;

import br.com.mastertech.cliente.DTO.ClientDTO;
import br.com.mastertech.cliente.model.Client;
import br.com.mastertech.cliente.repository.CLientRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    public CLientRepository clientRepository;

    public Client cadastrar(ClientDTO clientDTO) {
        Client client = new Client();
        client.setNome(clientDTO.getNome());
        client = clientRepository.save(client);

        return client;
    }

    public Client consultar(int id) throws NotFoundException {
        Optional<Client> clientOptional = clientRepository.findById(id);
        if (clientOptional.isPresent()) {
            Client client= clientOptional.get();
            return client;
        } else {
            throw new NotFoundException("Cliente não encontrado.");
        }
    }
}
