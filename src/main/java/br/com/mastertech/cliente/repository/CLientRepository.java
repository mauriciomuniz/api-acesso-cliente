package br.com.mastertech.cliente.repository;

import br.com.mastertech.cliente.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface CLientRepository extends CrudRepository<Client, Integer> {
}
